import React, { Component } from "react"
import './App.css';
import Home from './component/Home/Home';
import Search from './component/Search/Search';
import {BrowserRouter, Route, Switch } from "react-router-dom";
class App extends Component {
  render() {
    return (
      <div className="App">
          <BrowserRouter basename = '/fashion'>
            <Switch>
                <Route exact path="/">
                    <Home></Home>
                </Route>
                <Route exact path="/search">
                    <Search></Search>
                </Route>
            </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
