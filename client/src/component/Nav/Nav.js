import React, {Component} from 'react';
import './Nav.css';
import {withRouter} from "react-router-dom";
class Nav extends Component {
    constructor(props) {
        super(props);
        localStorage.setItem("keywords", ""); 
        this.state = {
            showNav: false,
            keywords: ""
        }
        this.onChangeKeys = this.onChangeKeys.bind(this);
    }
    onChangeKeys(e) {
        localStorage.setItem("keywords", JSON.stringify(e.target.value));
        this.setState({
            keywords: e.target.value
        })
    }

    

    submitClick = (e) => {
        e.preventDefault();
        if (this.state.keywords.length === 0) {
            alert("Please type some words");
        } else {
            this.setState({showNav: false});
            document.getElementById("overlay").classList.remove('open');
            document.getElementById("overlayBlur").classList.toggle('open');
            this.props.history.push("/search");
        }
    }

    btnClick = (e) => {
        e.preventDefault();
        if (this.state.showNav === false) {
            this.setState({showNav: true});
        } else {
            this.setState({showNav: false});
        }
        document.getElementById("overlay").classList.toggle('open');
        document.getElementById("overlayBlur").classList.toggle('open');
    }

    closeToggle = (e) => {
        e.preventDefault();
        if (this.state.showNav === false) {
            this.setState({showNav: true});
        } else {
            this.setState({showNav: false});
        }
        document.getElementById("overlay").classList.toggle('open');
        document.getElementById("overlayBlur").classList.toggle('open');
    }
    render() {
        return (
            <nav className="nav-bar row" style={{margin: '0'}}>
                <div className="apple-logo col-md-4" style={{textAlign:'left'}}>
                    <div className="nav-text">
                        ANASTASIA
                    </div>
                </div>
               
                <div className="search-container col-md-4">
                        <div>NEW</div>
                        <div>SHOP</div>
                        <div>SELL</div>
                        <div>ABOUT</div>
                </div>
                <div className="col-md-4" style={{justifyContent: 'flex-end', display: 'flex', alignItems:'center'}}>
                    <span>Eng</span>
                    <i className="fa fa-heart ml-5"></i>
                        <a type="button" id="toggle"  onClick = {this.btnClick.bind(this)}><i className="fa fa-search ml-5"></i></a>
                    <i className="fa fa-shopping-bag ml-5"></i>
                </div>
                <div className="overlay pr-5 pb-5" id="overlay">
                    <div className="pt-3 pr-3 row">
                        <div className="nav-text col-md-3" style={{color:'black'}}>
                            ANASTASIA
                        </div>
                        <div className="col-md-6 d-flex justify-content-center align-items-center">
                            <form  style={{alignItems:'center', width: '100%', position: 'relative'}} onSubmit={this.submitClick.bind(this)}>
                                    <input className="input-styles special-family" placeholder="Search Contents"  style={{fontSize: '1.2rem', color: 'black'}} onChange={this.onChangeKeys}></input>
                                    <a  className="searchbar-icon" style={{ color: 'black', fontSize: '1.7rem'}} type="submit"><i className="fa fa-search"></i></a>
                            </form>
                        </div>
                        <div className="col-md-3 d-flex justify-content-end">
                            <a type="button" onClick={this.closeToggle.bind(this)} id="closeT" className="close-btn d-flex justify-content-center align-items-center"><i style={{fontSize: '1.5rem', color: 'black'}} className="fa fa-close"></i></a>
                        </div>
                    </div>
                    <div className="d-flex justify-content-center align-items-center mt-4">
                        <div class="overlay-menu popular col-md-6">
                            <div className="popular-search-main">Popular Search Terms</div>
                            <ul className="mt-3">
                                <li><a href="#overview">Blue T-shirt</a></li>
                                <li className="mt-2"><a href="#styleguides">Topwear</a></li>
                                <li className="mt-2"><a href="#logo">Puma</a></li>
                                <li className="mt-2"><a href="#color-typography">Nike Shoes</a></li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                <div className="overlay-blur" id="overlayBlur" onClick={this.closeToggle.bind(this)}></div>

            </nav>

        )
    }
}

export default withRouter(Nav);